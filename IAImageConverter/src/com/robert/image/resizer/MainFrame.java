package com.robert.image.resizer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;


/**
 * @author Robert
 * @version Time:2013-10-24 ����5:45:15
 */
public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7682167834179504188L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int frameHeight = 400, frameWidth = 600;
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		
		MainFrame mainFrame = new MainFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(frameWidth, frameHeight);
		mainFrame.setLocation((screenDimension.width - frameWidth)/2, (screenDimension.height - frameHeight)/2);
		mainFrame.setTitle("Android/iOS ͼƬת����");
		
		MainPanel mainPanel = new MainPanel();

		mainFrame.setBackground(Color.white);
		mainFrame.setLayout(new BorderLayout());
		mainFrame.add(mainPanel, BorderLayout.CENTER);
		mainFrame.setVisible(true);
		
	}

}
