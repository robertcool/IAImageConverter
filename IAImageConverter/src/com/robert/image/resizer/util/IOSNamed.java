/**
 * 文件名称：IOSNamed.java
 * @since 2015-5-15 下午3:06:39
 * @author Robert
 */
package com.robert.image.resizer.util;

/**
 * @author Robert
 *
 */
public class IOSNamed extends Named {
	
	/**
	 * @param sourceName
	 */
	public IOSNamed(String sourceName) {
		super(sourceName);
	}
	
	
	/**
	 * 命名成iOS 一倍图
	 * */
	public IOSNamed renameTo1x(){
		String str = trim2x().trim3x().getSourceName();
		setSourceName(str);
		return this;
	}
	
	/**
	 * 命名成iOS 二倍图 即2x
	 * */
	public IOSNamed renameTo2x(){
		String str =  trim2x().trim3x().appendTo2x().getSourceName();
		setSourceName(str);
		return this;
	}
	
	/**
	 * 命名成iOS 二倍图 即2x
	 * */
	public IOSNamed renameTo3x(){
		String str = trim2x().trim3x().appendTo3x().getSourceName();
		setSourceName(str);
		return this;
	}

	
}
