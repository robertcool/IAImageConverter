/**
 * �ļ����ƣ�Named.java
 * @since 2015-5-15 ����2:58:57
 * @author Robert
 */
package com.robert.image.resizer.util;

/**
 * @author Robert
 *
 */
public class Named {
	
	private String sourceName;
	
	public Named(String sourceName){
		this.sourceName = sourceName;
	}

	public boolean isContain2x(String sourceName){
		return sourceName.contains("@2x.");
	}
	
	public boolean isContain3x(String sourceName){
		return sourceName.contains("@3x.");
	}
	
	public Named trim2x(){
		sourceName = sourceName.replaceAll("@2x", "");
		return this;
	}
	
	public Named trim3x(){
		sourceName = sourceName.replaceAll("@3x", "");
		return this;
	}
	
	public Named appendTo2x(){
		sourceName = sourceName.replace(".", "@2x.");
		return this;
	}
	
	public Named appendTo3x(){
		sourceName = sourceName.replace(".", "@3x.");
		return this;
	}

	public String getSourceName() {
		return sourceName;
	}

	protected void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	
	
	
}
