package com.robert.image.resizer.service;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.robert.image.resizer.util.IOSNamed;
import com.robert.image.resizer.util.Named;


import net.coobird.thumbnailator.Thumbnails;

/**
 * @author Robert
 * @version Time:2013-10-25 下午5:47:57
 */
public class ImageConvertService {
	
	private int totalFileCount = 0;
	
	private OnGeneratorListener listener;
	
	private boolean isImageXcassets = false;
	
	public ImageConvertService(){
		
	}
	
	public ImageConvertService(boolean useAssets){
		this.isImageXcassets = useAssets;
	}
	

	/**
	 * 图片转换
	 * @param srcDir 源文件目录
	 * @param desDir  生成文件目录
	 * @param phoneType 手机类型
	 * @param srcPicType  源图片大小类型
	 * @param desPicType 目标图片类型
	 * @throws IOException 
	 * */
	public  boolean convert(File srcDir, File desDir, PhoneType phoneType, SrcPicType srcPicType, DesPicType[] desPicType) throws IOException{
		if(srcDir == null){
			throw new RuntimeException("源文件夹不能为空");
		}else if(desDir == null){
			throw new RuntimeException("目标文件夹不能为空");
		}else if(phoneType == null){
			throw new RuntimeException("手机类型不能为空");
		}else if(srcPicType == null){
			throw new RuntimeException("源图片类型不能为空");
		}else if(desPicType == null){
			throw new RuntimeException("生成的图片类型不能为空");
		}
		
		//得到源文件图片个数
		File[] srcFiles = srcDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				if(pathname.getName().endsWith("png") || pathname.getName().endsWith("jpg") || pathname.getName().endsWith("jpeg")){
					return true;
				}else{
					return false;
				}
			}
		});
		//计算生成图片个数
//		totalFileCount = srcFiles.length*desPicType.length;
		totalFileCount = srcFiles.length;
		
		//目标文件夹
		File[] desFileDirs = new File[desPicType.length];
		if(phoneType == PhoneType.Android){
			for(int i = 0 ; i < desPicType.length; i++){
				DesPicType type = desPicType[i];
				desFileDirs[i] = this.createAndroidFolder(desDir, type);
			}
		}else if(!isImageXcassets){
			for(int i = 0 ; i < desPicType.length; i++){
				DesPicType type = desPicType[i];
				desFileDirs[i] = this.createIOSFolder(desDir, type);
			}
		}
		
		//为每个文件夹生成文件
		int progress = 0;
		
		for(int i = 0 ; i < srcFiles.length; i++){
			
			String fileName = srcFiles[i].getName();
			
			if(isImageXcassets){
				
				File dir = this.createAssetFolder(desDir, srcFiles[i]);
				String[] names = new String[desPicType.length];
				
				for(int t = 0; t < desPicType.length; t++){
					DesPicType type = desPicType[t];
					String desFileName = renameIOSPic(fileName, type);
					File desFile = new File(dir.getAbsolutePath() + File.separator + desFileName);
					
					names[t] = desFileName;
					
					double percent = this.getPercent(srcPicType, type);
					
					this.thumbnails(srcFiles[i], desFile, percent);	
				}
				
				this.writeToAssetJson(names, dir);
				
			}else{
				
				for(int t = 0 ; t < desFileDirs.length; t++){
					File dir = desFileDirs[t];
					DesPicType type = desPicType[t];
					String desFileName = "";
					if(phoneType == PhoneType.iOS){
						desFileName = renameIOSPic(fileName, type);
					}else{
						desFileName = fileName;
					}
					
					File desFile = new File(dir.getAbsolutePath() + File.separator + desFileName);
					
					double percent = this.getPercent(srcPicType, type);
					
					this.thumbnails(srcFiles[i], desFile, percent);
				}
				
			}
			
			progress++;
			if(listener != null){
				listener.onGenerator(progress, totalFileCount);
			}
			
		}
		
		return true;
	}
	
	private void writeToAssetJson(String[] fileNames, File jsonFileDir){
		File file = new File(jsonFileDir, "Contents.json");
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		sb.append("\"images\":[");
		
		//排序
		String[] idiom = new String[]{"\"universal\"","\"universal\"","\"universal\""};
		String[] scale = new String[]{"\"1x\"","\"2x\"","\"3x\""};
		String[] names = new String[3];
		
		for(String f : fileNames){
			Named named = new Named(f);
			if(named.isContain2x(f)){
				names[1] = f;
			}else if(named.isContain3x(f)){
				names[2] = f;
			}else{
				names[0] = f;
			}
		}
		
		for(int i = 0 ; i < 3; i++){
			String fileName = names[i];

			sb.append("{\"idiom\" : " + idiom[i] + ",");
			sb.append("\"scale\" : " + scale[i]);
			if(fileName != null){
				sb.append(",\"filename\" : \"" + fileName + "\"");
			}
			sb.append("}");
			if(i != 2){
				sb.append(",");
			}
		}
		
		sb.append("],");
		sb.append("\"info\":{");
		sb.append("\"version\":1,");
		sb.append("\"author\": \"xcode\"}}");
		
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			fos.write(sb.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(fos != null){
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
		
		
	}
	
	/**
	 * 创建Android的文件夹
	 * */
	private File createAndroidFolder(File desDir, DesPicType type){
		File file = null;
		if(type == DesPicType.Pic1){
			file = new File(desDir.getAbsolutePath() + File.separator + "drawable-mdpi");
		}else if(type == DesPicType.Pic1_5){
			file = new File(desDir.getAbsolutePath() + File.separator + "drawable-hdpi");
		}else if(type == DesPicType.Pic2){
			file = new File(desDir.getAbsolutePath() + File.separator + "drawable-xhdpi");
		}else if(type == DesPicType.Pic3){
			file = new File(desDir.getAbsolutePath() + File.separator + "drawable-xxhdpi");
		}
		createFolder(file);
		return file;
	}
	
	/**
	 * 创建iOS的文件夹
	 * */
	private File createIOSFolder(File desDir, DesPicType type){
		File file = null;
		if(type == DesPicType.Pic1){
			file = new File(desDir.getAbsolutePath() + File.separator + "Retina@1");
		}else if(type == DesPicType.Pic1_5){
			file = new File(desDir.getAbsolutePath() + File.separator + "Retina@1.5");
		}else if(type == DesPicType.Pic2){
			file = new File(desDir.getAbsolutePath() + File.separator + "Retina@2x");
		}else if(type == DesPicType.Pic3){
			file = new File(desDir.getAbsolutePath() + File.separator + "Retina@3x");
		}
		createFolder(file);
		return file;
	}
	
	/**
	 * 创建iOS Assset的文件夹（新版XCode支持）
	 * */
	private File createAssetFolder(File desDir, File file){
		String name = file.getName().substring(0, file.getName().lastIndexOf("."));
		File desFile = new File(desDir.getAbsoluteFile()  + File.separator + "Images.xcassets" + File.separator + name + ".imageset");
		createFolder(desFile);
		return desFile;
	}
	
	/**
	 * 创建文件夹
	 * */
	private void createFolder(File folder){
		if(folder != null && !folder.exists()){
			folder.mkdirs();
		}
	}
	
	
	/**
	 * 进行图片的缩放
	 * */
	private void thumbnails(File srcFile, File desFile, double percent){
		try {
			Thumbnails.of(srcFile).scale(percent).toFile(desFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private double getPercent(SrcPicType srcType, DesPicType desType){
		double d1, d2;
		if(srcType == SrcPicType.Pic1){
			d1 = 100.0;
		}else if(srcType == SrcPicType.Pic1_5){
			d1 =150.0;
		}else if(srcType == SrcPicType.Pic2){
			d1 =200.0;
		}else if(srcType == SrcPicType.Pic3){
			d1 =300.0;
		}else{
			throw new RuntimeException("源类型不匹配");
		}
		
		if(desType == DesPicType.Pic1){
			d2 = 100.0;
		}else if(desType == DesPicType.Pic1_5){
			d2 = 150.0;
		}else if(desType == DesPicType.Pic2){
			d2 = 200.0;
		}else if(desType == DesPicType.Pic3){
			d2 = 300.0;
		}else{
			throw new RuntimeException("目标类型不匹配");
		}
		
		return d2/d1;
	}
	
	private String renameIOSPic(String fileName, DesPicType desType) {
		String desFileName = fileName;

		if (desType == DesPicType.Pic1) {
			desFileName = new IOSNamed(desFileName).renameTo1x()
					.getSourceName();

		} else if (desType == DesPicType.Pic2) {
			desFileName = new IOSNamed(desFileName).renameTo2x()
					.getSourceName();

		} else if (desType == DesPicType.Pic3) {
			desFileName = new IOSNamed(desFileName).renameTo3x()
					.getSourceName();

		} else {
			desFileName = null;
		}

		return desFileName;

	}
	
		
	public void setOnGeneratorListener(OnGeneratorListener listener){
		this.listener = listener;
	}
	
	public interface OnGeneratorListener{
		public void onGenerator(int progress, int total);
	}
	
}
