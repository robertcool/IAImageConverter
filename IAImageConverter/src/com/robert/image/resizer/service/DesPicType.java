package com.robert.image.resizer.service;
/**
 * @author Robert
 * @version Time:2013-10-25 下午5:46:09
 * 输出的目标文件类型
 */
public enum DesPicType {

	/**
	 * 2倍
	 * */
	Pic2,
	/**
	 * 1.5倍
	 * */
	Pic1_5,
	/**
	 * 1倍
	 * */
	Pic1,
	/**
	 * 3倍
	 * */
	Pic3
}
