package com.robert.image.resizer.service;
/**
 * @author Robert
 * @version Time:2013-10-25 下午5:43:50
 * 源文件类型
 */
public enum SrcPicType {
	/**2倍*/
	Pic2, 
	/**1.5倍*/
	Pic1_5, 
	/**1倍*/
	Pic1, 
	/**3倍*/
	Pic3

}
