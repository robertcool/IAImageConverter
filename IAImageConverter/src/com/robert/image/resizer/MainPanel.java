package com.robert.image.resizer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

import com.robert.image.resizer.service.DesPicType;
import com.robert.image.resizer.service.ImageConvertService;
import com.robert.image.resizer.service.ImageConvertService.OnGeneratorListener;
import com.robert.image.resizer.service.PhoneType;
import com.robert.image.resizer.service.SrcPicType;


/**
 * @author Robert
 * @version Time:2013-10-24 下午5:52:44
 */
public class MainPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4331532130365800311L;
	
	
	private JTextField srcTextField, desTextField;
	private JFileChooser jFileChooser, jFileChooser2;
	private File selectFile;
	private File desFile;
	private ButtonGroup srcType, phoneType;
	//输出目录类型
	private JCheckBox[] jcbs;
	//源目录类型
	private JRadioButton jrb1,jrb2,jrb3,jrb4;
	//手机类型
	JRadioButton androidPhone = new JRadioButton("Android", true);
	JRadioButton iosPhone = new JRadioButton("iOS");
	
	//iOS Images.xcassets
	JCheckBox imageXcassetsCb;
	
	private JButton generationBtn;
	private JLabel generationLabel;
	
	private JProgressBar progressBar;
	
	public MainPanel() {
		this.setBackground(Color.white);
		this.initViews();
	}

	

	private void initViews(){
		jFileChooser = new JFileChooser();
		jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		jFileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
		jFileChooser2 = new JFileChooser();
		jFileChooser2.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		jFileChooser2.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
		
		this.setLayout(new BoxLayout(MainPanel.this, BoxLayout.Y_AXIS));
		JPanel panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
//		this.add(panel1);
		
		JLabel jLabel = new JLabel("源图片目录：");
		panel1.add(jLabel, BorderLayout.WEST);
		
		
		srcTextField = new JTextField();
		srcTextField.setEditable(false);
		panel1.add(srcTextField, BorderLayout.CENTER);
		
		JButton jButton = new JButton("选择源目录");
		panel1.add(jButton, BorderLayout.EAST);
		jButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int status = jFileChooser.showOpenDialog(MainPanel.this);
				//选择文件成功
				if(status == JFileChooser.APPROVE_OPTION ){
					selectFile = jFileChooser.getSelectedFile();// f为选择到的文件  
					srcTextField.setText(selectFile.getAbsolutePath());  
					
					desFile = new File(selectFile.getAbsoluteFile() + "/des_image");
					jFileChooser2.setCurrentDirectory(desFile);
					desTextField.setText(desFile.getAbsolutePath());
				}
			}
		});
		
		JPanel panel2 = new JPanel(new GridLayout(1,5));
		JLabel label2 = new JLabel("源图片大小：");
		srcType = new ButtonGroup();
		jrb1 = new JRadioButton("2倍",true);
		jrb2 = new JRadioButton("1.5倍");
		jrb3 = new JRadioButton("1倍");
		jrb4 = new JRadioButton("3倍");
		jrb1.setBackground(Color.white);
		jrb2.setBackground(Color.white);
		jrb3.setBackground(Color.white);
		jrb4.setBackground(Color.white);
		srcType.add(jrb1);
		srcType.add(jrb2);
		srcType.add(jrb3);
		srcType.add(jrb4);
		panel2.add(label2, 0,0);
		panel2.add(jrb1,0,1);
		panel2.add(jrb2,0,2);
		panel2.add(jrb3,0,3);
		panel2.add(jrb4,0,4);
		panel1.setBackground(Color.white);
		panel2.setBackground(Color.white);
		
		
		JPanel srcPanel = new JPanel(new GridLayout(2,1));
		srcPanel.add(panel1);
		srcPanel.add(panel2);
		
		this.add(srcPanel);
		
		
		JPanel desPanel = new JPanel(new GridLayout(3,1));
		this.add(desPanel);
		
		JPanel panel3 = new JPanel(new BorderLayout());
		JLabel label3 = new JLabel("目标目录：");
		desTextField = new JTextField();
		desTextField.setEditable(false);
		JButton button3 = new JButton("选择目标目录");
		
		panel3.add(label3, BorderLayout.WEST);
		panel3.add(desTextField, BorderLayout.CENTER);
		panel3.add(button3, BorderLayout.EAST);
		panel3.setBackground(Color.white);
		button3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int status = jFileChooser2.showOpenDialog(MainPanel.this);
				//选择文件成功
				if(status == JFileChooser.APPROVE_OPTION ){
					desFile = jFileChooser2.getSelectedFile();// f为选择到的文件  
					desTextField.setText(desFile.getAbsolutePath());  
				}
			}
		});
		
		desPanel.add(panel3);
		
		JPanel panel4 = new JPanel(new FlowLayout());
		JLabel label4 = new JLabel("目标类型：");
		panel4.add(label4);
		
		jcbs = new JCheckBox[4];
		String jcbTexts[] = new String[]{"1倍","1.5倍","2倍","3倍"};
		for(int i = 0 ; i < jcbs.length ; i ++){
			jcbs[i] = new JCheckBox(jcbTexts[i], true);
			jcbs[i].setBackground(Color.white);
			panel4.add(jcbs[i]);
		}
		
		panel4.setBackground(Color.white);
		desPanel.add(panel4);
		
		JPanel panel5 = new JPanel(new FlowLayout());
		
		phoneType = new ButtonGroup();
		androidPhone.setBackground(Color.yellow);
		iosPhone.setBackground(Color.yellow);
		phoneType.add(androidPhone);
		phoneType.add(iosPhone);
		panel5.add(androidPhone);
		panel5.add(iosPhone);
		
		imageXcassetsCb = new JCheckBox("使用Imagex.cassets(ios专用)", false);
		imageXcassetsCb.setBackground(Color.white);
		panel5.add(imageXcassetsCb);
		
		panel5.setBackground(Color.white);
		desPanel.add(panel5);
		
		JPanel btnPanel = new JPanel(new GridLayout(3,1));
		
		generationBtn = new JButton("开始生成");
		btnPanel.add(generationBtn,0,0);
		progressBar = new JProgressBar(0, 100);
		btnPanel.add(progressBar,1,0);
		generationBtn.addActionListener(clickListener);
		generationLabel = new JLabel();
		btnPanel.add(generationLabel);
		
		generationLabel.setVisible(false);
		progressBar.setVisible(false);
		
		this.add(btnPanel);
		
		
		
		srcPanel.setBorder(BorderFactory.createTitledBorder("选择源目录"));
		desPanel.setBorder(BorderFactory.createTitledBorder("选择目标目录"));
		btnPanel.setBorder(BorderFactory.createTitledBorder("操作"));
		
		srcPanel.setBackground(Color.white);
		desPanel.setBackground(Color.white);
		btnPanel.setBackground(Color.white);
	}

	
	private ActionListener clickListener = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			
			final String srcDir = srcTextField.getText(); //源目录
			final String desDir = desTextField.getText(); //目标目录
			final SrcPicType srcType; //源类型
			final List<DesPicType> desType = new ArrayList<DesPicType>();//目标类型
			final PhoneType pt;
			
			if(jrb1.isSelected()){
				srcType = SrcPicType.Pic2;
			}else if(jrb2.isSelected()){
				srcType = SrcPicType.Pic1_5;
			}else if(jrb3.isSelected()){
				srcType = SrcPicType.Pic1;
			}else{
				srcType = SrcPicType.Pic3;
			}
			
			if(jcbs[0].isSelected()){
				desType.add(DesPicType.Pic1);
			}
			if(jcbs[1].isSelected()){
				desType.add(DesPicType.Pic1_5);
			}
			if(jcbs[2].isSelected()){
				desType.add(DesPicType.Pic2);
			}
			if(jcbs[3].isSelected()){
				desType.add(DesPicType.Pic3);
			}
			if(desType.size()<=0){
				JOptionPane.showMessageDialog(null, "没有选择目标类型", "错误", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			if(androidPhone.isSelected()){
				pt = PhoneType.Android;
			}else{
				pt = PhoneType.iOS;
			}
			
			if(srcDir == null || srcDir.trim().isEmpty()){
				JOptionPane.showMessageDialog(null, "没有选择源文件目录", "错误", JOptionPane.ERROR_MESSAGE);
				return;
			}else if(desDir == null || desDir.trim().isEmpty()){
				JOptionPane.showMessageDialog(null, "没有选择目标文件目录", "错误", JOptionPane.ERROR_MESSAGE);
				return;
			}

			progressBar.setVisible(true);
			generationLabel.setVisible(true);
			generationBtn.setEnabled(false);
			progressBar.setValue(0);
			
			generationLabel.setText("开始生成，进度：0%");
			new Thread(new Runnable(){
				@Override
				public void run() {
					ImageConvertService service = new ImageConvertService(imageXcassetsCb.isSelected());
					
					service.setOnGeneratorListener(new OnGeneratorListener() {
						
						@Override
						public void onGenerator(int progress, int total) {
							int value = (int)(progress*100/total);
							progressBar.setValue(value);
							generationLabel.setText("正在生成，进度：" +value+ "%");
							generationLabel.setText("完成，进度：" +value+ "%");
						}
					});
					
					try {
						DesPicType[] dt = new DesPicType[desType.size()];
						for(int i = 0; i < desType.size(); i++){
							dt[i] = desType.get(i);
						}
						service.convert(new File(srcDir), new File(desDir), pt, srcType, dt);
					} catch (IOException e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null, e1.getMessage(), "错误", JOptionPane.ERROR_MESSAGE);
					}
					

					generationBtn.setEnabled(true);
				}
			}).start();
			
		}
		
	};
}
