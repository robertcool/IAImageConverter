打包好的工具，放到项目的附件中。大家可以下载

#IAImageConverter

<img src="http://git.oschina.net/robertcool/IAImageConverter/raw/master/IAImageConverter/images/1.png"></img>

==========转化前的图片=======
<br/>
<br>
=========［下面根据不同情况生成图片］=======<br><br>
<img src="http://git.oschina.net/robertcool/IAImageConverter/raw/master/IAImageConverter/images/2.png"></img>

<img src="http://git.oschina.net/robertcool/IAImageConverter/raw/master/IAImageConverter/images/3.png"></img>

<img src="http://git.oschina.net/robertcool/IAImageConverter/raw/master/IAImageConverter/images/4.png"></img>

<img src="http://git.oschina.net/robertcool/IAImageConverter/raw/master/IAImageConverter/images/5.png"></img>

==================【20150515】=====================================

增加新版XCode的Image.xcassets图片集合的生成

==================【20131107】======================================

用于移动开发时，iOS或者Android的切图转化

==================【20131114】=====================================

使用Java自带的图片转换，失真太严重。

在网上找到更好的第三方图片转换工具，Thumbnailator。

效果不错！

项目地址：http://code.google.com/p/thumbnailator/